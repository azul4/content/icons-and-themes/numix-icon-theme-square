# numix-icon-theme-square 
 
Square is an icon theme for Linux from the Numix project

https://github.com/numixproject/numix-icon-theme-square

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/icons-and-themes/numix-icon-theme-square.git
```


